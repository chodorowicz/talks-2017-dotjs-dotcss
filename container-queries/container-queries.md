# Jonathan Snook - Container Queries

- from Shopify
- [dotCSS 2017 - Jonathan Snook - Container Queries - YouTube](https://www.youtube.com/watch?v=s1J4T1NW3qo)

---

# Issue

- same page size, same compontent, different versions
  - [2 column layout](https://youtu.be/s1J4T1NW3qo?t=4m12s)
  - [1 column layout](https://youtu.be/s1J4T1NW3qo?t=4m33s)
- > with media queries you need to now interplay all objects on the page in all scenarios

---

# Solution - container queries

- no spec yet
- polyfills (prolyfill - future polyfills)

- [ResponsiveImagesCG/cq-demos: Demonstrations of Element queries](https://github.com/ResponsiveImagesCG/cq-demos)
- [ausi/cq-prolyfill: Prolyfill for CSS Container Queries](https://github.com/ausi/cq-prolyfill)

```
.element:media( min-width: 500px ) {}
```

- https://github.com/Snugug/eq.js/

# circular reference problem

---

.element:media(min-width: 500px) {
  width: 400px;
}

- limit loop to several times?
- browser wouldn't know which queries to apply until layout phase
  - go back to style phase

![](./browser-phase.png)

---

# things which could help

- resize observer
- houdini
