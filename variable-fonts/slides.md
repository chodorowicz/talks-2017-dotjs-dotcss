# Variable Fonts - Laurence Penney

- [Variable fonts: a million times the possibilities..](https://www.youtube.com/watch?v=63Uv_VC7nbY)

---

# the biggest change in Typograhpy in years

- most exciting in typography in 20 years
- 1996

![](image-01-core-fonts.png)

---

# 2010

![](image-02-typo.png)

---

# font families

- 259 fonts (Sharp Grotesk)
![](image-03-tofino-font.png)

---

# 2016 

## open type specification 1.8 - variable fonts 🎉

- Atypl conference Warsaw 2016
- Microsoft Adobe, Apple and Google.

---

# variable fonts

- interpolating between master designs in a multi-dimensional design space
- tweening beteen keyframas
- axes: https://youtu.be/63Uv_VC7nbY?t=4m6s
- interpolation: https://youtu.be/63Uv_VC7nbY?t=6m37s


---

# Demo fonts

- [Axis-Praxis: OpenType variable fonts in the browser](https://www.axis-praxis.org/specimens/)

- dunbar
- zycon
- Grade
- FTW fit-to-width

--- 

# API

- controllable dynamically by
  - JS
    - https://youtu.be/63Uv_VC7nbY?t=14m44s
  - css transition
    - https://youtu.be/63Uv_VC7nbY?t=14m55s

# Interfaces

- https://youtu.be/63Uv_VC7nbY?t=15m44s


