# Glimmer

- Tom Dale🕴️
- [Glimmer is about components](https://youtu.be/nXCSloXZ-wc?t=4m6s)
  - like React
  - Ember is a whole stack framework
  - written in TS

# speed
  - phases
    - download
    - *parse*
    - *compile*
    - evaluate
  - https://youtu.be/nXCSloXZ-wc?t=6m24s
  - especially large difference for mobile, 2-5x longer

# glimmer - compiler
  - bytecode, assembly language
    - https://youtu.be/nXCSloXZ-wc?t=11m10s
  - vm in the browser
- comparison to Angular template size
  - https://youtu.be/nXCSloXZ-wc?t=12m26s

# demo

- https://youtu.be/nXCSloXZ-wc?t=16m46s

# incremental rendering

- demo https://youtu.be/nXCSloXZ-wc?t=19m39s

# optimised updates

- useless rerenders (reconciliations)
- Glimmer has *shouldComponentUpdate* by default on each component
- automatic generation of update function
  - https://youtu.be/nXCSloXZ-wc?t=28m34s